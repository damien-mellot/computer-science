from flask import Flask, jsonify, request


app = Flask(__name__)
PORT = 5000
HOST = "0.0.0.0"


@app.route("/hello_world", methods=["GET"])
def hello_world():
    response = {"hello": "world"}
    return jsonify(response)

@app.route("/example", methods=["GET"])
def example():
    try:
        initial_number = request.get_json()["question"]
        answer = float(initial_number)*2
    except (ValueError, TypeError, KeyError):
        DEFAULT_RESPONSE = 0
        answer = DEFAULT_RESPONSE
    response = {"answer": answer}
    return jsonify(response)


if __name__ == "__main__":
    app.run(debug=False, host=HOST, port=PORT)
